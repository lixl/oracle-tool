# 1.install到本地
mvn install:install-file -DgroupId=org.oracle -DartifactId=ojdbc14 -Dversion=10.2.01 -Dpackaging=jar -Dfile=E:\ojdbc14.jar

# 2.deploy到nexus私服
mvn deploy:deploy-file -DgroupId=org.oracle -DartifactId=ojdbc14 -Dversion=10.2.01 -Dpackaging=jar -Dfile=E:\ojdbc14.jar -Durl=http://nexus.sysware.com.cn/repository/maven-releases/ -DrepositoryId=maven-releases

# 注意
直接通过nexus界面上传有时候会下载不到，原因待查。。。
可能与neuxs3有关（nexus2没问题）