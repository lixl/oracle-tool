package com.sysware.p2m.utils;

import org.junit.Assert;
import org.junit.Test;

public class DBPropertiesUtilTest {

    @Test
    public void getValue(){
        String value = DBPropertiesUtil.getValue(DBPropertiesUtil.DB_DATABASE_KEY);
        Assert.assertNotNull(value);
    }

}
