package com.sysware.p2m.log4j;

import org.apache.log4j.FileAppender;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeFileAppender extends FileAppender {

    public TimeFileAppender() throws IOException{

    }

    private String datePattern;

    public static final String DEFAULT_FILE_EXTENSION = ".log";

    @Override
    public void activateOptions() {
        this.layout = getLayout();
        String fileName = getFile();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(getDatePattern());
        String dataStr = simpleDateFormat.format(new Date());
        this.setFile(fileName+dataStr+DEFAULT_FILE_EXTENSION);
        super.activateOptions();
    }

    public String getDatePattern() {
        return datePattern;
    }

    public void setDatePattern(String datePattern) {
        this.datePattern = datePattern;
    }
}
