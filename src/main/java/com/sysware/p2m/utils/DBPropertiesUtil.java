package com.sysware.p2m.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class DBPropertiesUtil {

    public static final String DB_DATABASE_KEY = "db.database";
    public static final String DB_DBA_NAME_KEY = "db.dba.name";
    public static final String DB_DBA_PASSWORD_KEY = "db.dba.password";
    public static final String DB_USER_NAME_KEY = "db.user.name";
    public static final String DB_USER_PASSWORD_KEY = "db.user.password";

    private static final String DB_CONFIG_FILE = "conf/db.properties";

    public static Logger logger = LoggerFactory.getLogger(DBPropertiesUtil.class);

    private static Properties properties = new Properties();

    static {
        File file = new File(DB_CONFIG_FILE);
        if (!file.exists()) {
            file = new File("..", DB_CONFIG_FILE);
        }
        loadProperties(file);
    }

    private static void loadProperties(File file) {
        logger.debug(file.getAbsolutePath() + " exists " + file.exists());
        try {
            InputStream is = file.toURI().toURL().openStream();
            properties.load(is);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Properties getProperties(){
        return properties;
    }

    public static String getValue(String key) {
        return getProperties().getProperty(key, "");
    }

}
