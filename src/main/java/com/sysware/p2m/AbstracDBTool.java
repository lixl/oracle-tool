package com.sysware.p2m;

import com.sysware.p2m.utils.DBPropertiesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.Scanner;
import java.util.Set;

public abstract class AbstracDBTool {

    static Logger logger = LoggerFactory.getLogger(OracleTool.class);

    public static int validateProperties() {
        Properties properties = DBPropertiesUtil.getProperties();
        logger.info("-- listing properties --");
        for (String key : properties.stringPropertyNames()) {
            logger.info("{}\t{}", key, properties.getProperty(key));
        }

        System.out.print("确定执行(yes/no)?");
        Scanner scanner = new Scanner(System.in);
        String inputStr = scanner.next();
        if ("yes".equals(inputStr)) {
            return 1;
        } else if ("no".equals(inputStr)) {
            return 2;
        } else {
            return 0;
        }
    }

    public static String getConfigUrl() {
        return "jdbc:oracle:thin:@//" + getDBDatabase();
    }

    public static String getConfigDBAUser() {
        return DBPropertiesUtil.getValue(DBPropertiesUtil.DB_DBA_NAME_KEY);
    }

    public static String getConfigDBAPassword() {
        return DBPropertiesUtil.getValue(DBPropertiesUtil.DB_DBA_PASSWORD_KEY);
    }

    public static String getConfigUser() {
        return DBPropertiesUtil.getValue(DBPropertiesUtil.DB_USER_NAME_KEY);
    }

    public static String getConfigPassword() {
        return DBPropertiesUtil.getValue(DBPropertiesUtil.DB_USER_PASSWORD_KEY);
    }

    private static String getDBDatabase() {
        return DBPropertiesUtil.getValue(DBPropertiesUtil.DB_DATABASE_KEY);
    }

}
