# oracle-tool

## 项目介绍
通过jdbc进行oracle管理的小工具（基于jdk7,oracle11g）			

目前支持			

①创建用户		

②删除用户		

③执行sql		

## 安装教程

###1. 如何打包 
执行mvn clean package
###2. xxxx

###3. xxxx

## 目录结构说明

解压*.zip文件后，得到三个目录（bin、conf、lib）

###1. bin   

createUser.bat 创建oracle用户(在conf/db.properties中配置)   

dropUser.bat   

executeSql.bat   

###2. conf (db.properties参数说明)   

db.database=192.168.40.154:1521/helowin 数据库连接地址   

db.dba.name=system 管理员账户   

db.dba.password=helowin 管理员密码   

db.user.name=P2M_P624_20180108 数据库操作用户名   

db.user.password=P2M_P624_20180108 数据库操作用户密码		

###3. lib

所有jar依赖		

## 开发进度报告
### 20180620 
计划用mybatis实现数据库连接，框架已经搭好，还需要处理的点：		

1、删除用户和执行SQL功能需要补充		

2、事务的控制 		
(如：创建用户时，创建表空间、创建用户、给用户授权，该过程中出错如何回滚)

